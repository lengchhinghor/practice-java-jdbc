package com.company;
import java.sql.*;

public class Quiz {

    static final String SQL_INSERT = "INSERT INTO QUIZ (student_id,title,subject,date, score) VALUES (?,?,?,?,?)";
    static final String deleteQuiz = "DELETE FROM quiz WHERE student_id = ?";
    static final String updateQuiz = "UPDATE quiz SET name = ? WHERE student_id = ?";
    static final String displayQuiz = "SELECT * FROM quiz ";
    static void create(){
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres", "postgres", "Chhinghor7944!");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_INSERT)) {

            preparedStatement.setInt(1, 2);
            preparedStatement.setString(2, "English for children");
            preparedStatement.setString(3, "IT");
            preparedStatement.setString(4, "2001-01-1999");
            preparedStatement.setString(5, "150");

            int row = preparedStatement.executeUpdate();
            // rows affected
            System.out.println("create at row " +row); //1

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void update(){
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres", "postgres", "Chhinghor7944!");
             PreparedStatement preparedStatement = conn.prepareStatement(updateQuiz)) {

            preparedStatement.setString(1, "ChhinghorUpdate");
            preparedStatement.setInt(2, 1);
            int row = preparedStatement.executeUpdate();
            // rows affected
            System.out.println("update at row"+ row); //1

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void delete(){
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres", "postgres", "Chhinghor7944!");
             PreparedStatement preparedStatement = conn.prepareStatement(deleteQuiz)) {

//          preparedStatement.setString(1, "ChhinghorUpdate");
            preparedStatement.setInt(1, 1);
            int row = preparedStatement.executeUpdate();
            // rows affected
            System.out.println("delete at row"+row); //1

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    static void display(){
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres", "postgres", "Chhinghor7944!");
             PreparedStatement preparedStatement = conn.prepareStatement(displayQuiz)) {

         ResultSet rs = preparedStatement.executeQuery();
            System.out.println("Display all students records");
            while (rs.next()) {
                System.out.print(rs.getInt(1));
                System.out.print(": ");
                System.out.println(rs.getString(2));
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
    }

    public static void main(String[] args) {

    create();
    update();
    delete();
    display();

    }

}