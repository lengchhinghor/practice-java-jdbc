package com.company;
import java.sql.*;

public class Student {

    static final String SQL_INSERT = "INSERT INTO STUDENTS (name,gender,card_id,generation_id) VALUES (?,?,?,?)";
    static final String deleteStudent = "DELETE FROM students WHERE student_id = ?";
    static final String updateStudent = "UPDATE students SET name = ? WHERE student_id = ?";
    static final String displayStudent = "SELECT * FROM students INNER JOIN quiz ON quiz.student_id = students.student_id";
    static void create(){
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres", "postgres", "Chhinghor7944!");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_INSERT)) {

            preparedStatement.setString(1, "Chhinghor");
            preparedStatement.setString(2, "Male");
            preparedStatement.setString(3, "0001");
            preparedStatement.setString(4, "0009");
            int row = preparedStatement.executeUpdate();
            // rows affected
            System.out.println("create at row " +row); //1

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void update(){
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres", "postgres", "Chhinghor7944!");
             PreparedStatement preparedStatement = conn.prepareStatement(updateStudent)) {

            preparedStatement.setString(1, "ChhinghorUpdate");
            preparedStatement.setInt(2, 1);
            int row = preparedStatement.executeUpdate();
            // rows affected
            System.out.println("update at row"+ row); //1

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void delete(){
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres", "postgres", "Chhinghor7944!");
             PreparedStatement preparedStatement = conn.prepareStatement(deleteStudent)) {

//          preparedStatement.setString(1, "ChhinghorUpdate");
            preparedStatement.setInt(1, 1);
            int row = preparedStatement.executeUpdate();
            // rows affected
            System.out.println("delete at row"+row); //1

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    static void display(){
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres", "postgres", "Chhinghor7944!");
             PreparedStatement preparedStatement = conn.prepareStatement(displayStudent)) {

         ResultSet rs = preparedStatement.executeQuery();
            System.out.println("Display all students records");
            while (rs.next()) {
                System.out.print(rs.getInt(1));
                System.out.print(": ");
                System.out.println(rs.getString(2));
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
    }

    public static void main(String[] args) {

    create();
//    update();
//    delete();
//    display();

    }

}